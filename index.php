<?php

spl_autoload_register(function ($class) {/*Permet de charger automatiquement les classes des fichiers.php sans devoir utiliser des require!!*/
    require __DIR__ . '/' . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});

//todo 6 : ajoutez la possibilité d'esquiver et de parer pour les personnages.
// les attaques des armes de corps à corps ne permettent pas d'esquive, mais permettent la parade si le personnagepossède une arme défensive
// method associés aux personnages à rajouter esquive + paré, if getweapon (le défenseur ne peut pas esquiver) if npc/pc character
// boucles if pc/npc character get weapon=> catergory=> esquive desactiver si l'arme c à c
// le paramètre de la parade(du défenseur ne doit pas être activée) if(le personnage défenseur possède une
// arme défensive categorie,type)
// les armes à distance permettent l'esquive, mais pas la parade. //c à c pas d'esquive,// Les armes à
// distance permettent l'esquive, de l'attaquant+opérateur de comparaison 2if imbriqués
//




//todo 7 : Si un personnage possède une capacité magique offensive, l'attaque standard est remplacée par une attaque magique, ignorant la parade et l'esquive
// Si un personnage possède une capacité magique défensive, la valeur de défense de la capacité magique s'ajoute à la parade ou à l'esquive
//if getmagic off attack

//todo 8 : esquive et parade
// l'esquive accorde 10% de chance d'éviter une attaque par tranche de 10 points en valeur de défense.// Si on a une défence de 10 on à une chance sur 10 d'éviter l'attaque
// créer une method(valeur de défense du même type que l'attribut) pour esquive + une pour parade
// dans la method esquive(*0,1) et la method parade(*0,1)
// faire test si tranche de défense a 10 alors 10% d'esquive (par tranche de 10 points tu ajoutes 10%) =>
// 19 points =10% , 20 points =20%
// la parade accorde 10% de chance de riposter (le défenseur effectue une attaque contre l'attaquant, en plus de ses attaques normales) par tranche de 10 points en valeur d'attaque.


// lorsqu'un pc ou un npc est généré, il doit recevoir une arme aléatoire.
// lorsqu'un pc ou un npc est généré, il doit recevoir une capacité magique aléatoire.
$weapons = \classes\weapon::generateWeapons();/*pour appeler une method static, on utilise :: Qui vient de weapons.php */
$magics = \classes\magic::generateMagic();

/*création nouveau personnage*/
/*définir la valeur des attributs*/
$pc = new \classes\pc('PC', 1,'McManon');/*Affectation d'une instance de la classe (objet unique) à valeur à la
variable $pc. Type objet*/
/*$pc->setPseudo('McManon');/*création d'un pseudo*/
$pc->setHp(20);/*mise à jour de la method setHp des attributs de la class pc via la méthode setter*/
$pc->setAttack(10);/*+ objet mise à jour de la method setAttack des attributs de la class attack*/
$pc->setDefense(10);/*+ Définir la valeur de l'attribut de defense via la method setDefense, qui sera à 10*/
$pc->giveWeapon($weapons);/*Qui vient de la variable qui vient de la method static + générer les armes + pas mettre de new quand c'est du static*/
$pc->giveMagic($magics);

/*création nouveau personnage d'une autre classe que pc*/
/*définir la valeur des attributs*/
$npc = new \classes\npc('NPC', 2);/*Affectation pour devenir un objet $npc*/
$npc->setHp(20);
$npc->setAttack(10);
$npc->setDefense(10);
$npc->giveWeapon($weapons);/*Qui vient de la variable qui vient de la method static + générer les armes + pas mettre de new quand c'est du static*/
$npc->giveMagic($magics);

// attaque simple
echo $pc->Attack($npc);/*Le npc se fait attaquer par le pc sous forme de string*/

// attaque & riposte jusqu'à la mort d'un des candidats
while ($pc->getHp() > 0 && $npc->getHp() > 0) {/*Tant que supérieur à 0 au niveau des pc et npc*/
    echo $pc->Attack($npc);
    if ($pc->getHp() > 0 && $npc->getHp() > 0) {/*Si supérieur à zéro au niveau de npc et pc*/
        echo $npc->Attack($pc);
    }
}
// combat terminé, le vainqueur
if ($pc->getHp() == 0) {/*Si les points de vie attaquant = 0*/
    $winner = $npc->getName();/*le gagnant est npc car pc = 0*/
} else {
    $winner = $pc->getName();/*le gagnant est pc car npc = 0*/
}
echo '<p>Le combat est terminé! Le vainqueur est : ' . $winner . '</p>';/*Le nom de la variable*/


// le NPC ne sera plus déterminé par le script index, mais proviendra d'une base de données où l'adversaire sera déterminé aléatoirement