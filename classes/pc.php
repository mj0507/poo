<?php

namespace classes;

use classes\character;

class pc extends character/*Tout ce qui se trouve dans character se trouvera dans pc*/
{
    public string $pseudo; //todo : ajoutez une propriété publique "pseudo"


    /**
     * @param string $name
     * @param int $id
     * @param string $pseudo
     */
    /*N'est possible qu'avec la classe extends pour ajouter l'attribut pseudo au construct venant du parent character*/
    public function __construct(string $name, int $id,string $pseudo)
    {
        parent::__construct($name,$id);
        $this->pseudo = $pseudo;
        $this->name = $pseudo;
    }

    /**
     * @return string
     */


    public function getPseudo(): string
    {
        return $this->pseudo;/*Instance de l'objet de la classe pc*/
    }

    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo): void
    {
        $this->pseudo = $pseudo;
    }

}