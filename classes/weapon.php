<?php
/*Donner le type d'armes : Name, min damage, max damage et la faire intervenir dans le combat index*/

namespace classes;

class weapon extends capacity /*classe*/ /*étendue*/
{
    public int           $type;
    private static array $types = [self::CAT_MELEE, self::CAT_RANGED];

    const CAT_MELEE  = 1;// corps à corps
    const CAT_RANGED = 2;// distance


    /**
     * @param int $id
     * @param string $name
     * @param int $minDamage
     * @param int $maxDamage
     * @param int $category
     * @param int $type
     */
    public function __construct(int $id, string $name, int $minDamage, int $maxDamage, int $category = self::CAT_OFFENSIVE, int $type = self::CAT_MELEE)
    {
        parent::__construct($id, $name, $minDamage, $maxDamage, $category);//$type=propriété de la static $types
        if (!in_array($type, self::$types)) {
            $type = self::CAT_MELEE;
        }
        $this->type = $type;
    }

    public function getType(): int {
        return $this->type;
    }

    public function setType(int $type): void {
        $this->type = $type;
    }


//todo créez au moins 4 armes différentes.


    public static function generateWeapons(): array/*Method static qui sera utilisée de façon statique, mais qui n'a rien avoir avec l'objet
    Permet d'instancier (de créer) 4 armes dans notre tableau*/ {
        /*Création de nouvelles armes sur la même classe*/
        $revolver = new \classes\weapon(1, 'revolver', 2, 7, self::CAT_OFFENSIVE, self::CAT_RANGED);/*() les paramètres
     du
    construct*/
        $cut    = new \classes\weapon(2, 'couteau', 1, 4, self::CAT_DEFENSIVE,self::CAT_MELEE);/*() les paramètres du construct*/
        $cutter = new \classes\weapon(3, 'cutter', 1, 3);/*() les paramètres du construct*/
        $aka47  = new \classes\weapon(4, 'aka47', 4, 10,self::CAT_OFFENSIVE,self::CAT_RANGED);/*() les paramètres du construct*/
        return [$aka47, $cutter, $cut,
                $revolver]; /*Création d'un tableau pour une utilisation simple pour gérer l'utilisation*/

    }


    /*    Method toto de la class capacity, override une method(surcharge-ajout) par exemple afficher min damage
        Changer le comportement
        private function toto(string $maxDamage): void
        {
            echo $maxDamage;
        }*/
}