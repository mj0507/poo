<?php

namespace classes;
use classes\character;
//todo 3 : créez un nouvel élément (class) : les capacités magiques (que vous pouvez appeler simplement "magic")

class magic
{
    private int $id;
    public string $name;
    public int $min_damage;
    public int $max_damage;

    /**
     * @param int $id
     * @param string $name
     * @param int $min_damage
     * @param int $max_damage
     */
    public function __construct(int $id, string $name, int $min_damage, int $max_damage)
    {
        $this->id = $id;
        $this->name = $name;
        $this->min_damage = $min_damage;
        $this->max_damage = $max_damage;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getMinDamage(): int
    {
        return $this->min_damage;
    }

    /**
     * @param int $min_damage
     */
    public function setMinDamage(int $min_damage): void
    {
        $this->min_damage = $min_damage;
    }

    /**
     * @return int
     */
    public function getMaxDamage(): int
    {
        return $this->max_damage;
    }

    /**
     * @param int $max_damage
     */
    public function setMaxDamage(int $max_damage): void
    {
        $this->max_damage = $max_damage;
    }

    public static function generateMagic(): array
    {
        $sleep = new \classes\magic(1, 'dodo', 1, 0,self::CAT_DEFENSIVE,5);
        $invisibility = new \classes\magic(2, 'invisibilité', 1, 0);
        $fog = new \classes\magic(3, 'brouillard', 1, 0);
        $freeze = new \classes\magic(4, 'geler', 3, 0);
        return [$sleep, $invisibility, $fog, $freeze];
    }
}