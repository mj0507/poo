<?php

namespace classes;
/**
 * Que sont les espaces de noms ? Dans leur définition la plus large, ils représentent un moyen d'encapsuler des éléments.
 * Cela peut être conçu comme un concept abstrait, pour plusieurs raisons. Par exemple, dans un système de fichiers,
 * les dossiers représentent un groupe de fichiers associés et servent d'espace de noms pour les fichiers qu'ils contiennent.
 * Un exemple concret est que le fichier foo.txt peut exister dans les deux dossiers /home/greg et /home/other,
 * mais que les deux copies de foo.txt ne peuvent pas co-exister dans le même dossier.
 * De plus, pour accéder au fichier foo.txt depuis l'extérieur du dossier /home/greg,
 * il faut préciser le nom du dossier en utilisant un séparateur de dossier, tel que /home/greg/foo.txt.
 * Le même principe s'applique aux espaces de noms dans le monde de la programmation.
 *
 * Dans le monde PHP,
 * les espaces de noms sont conçus pour résoudre deux problèmes que rencontrent les auteurs de bibliothèques et d'applications lors de la réutilisation d'éléments tels que des classes ou des bibliothèques de fonctions :
 * - Collisions de noms entre le code que vous créez, les classes, fonctions ou constantes internes de PHP, ou celle de bibliothèques tierces.
 * - La capacité de faire des alias ou de raccourcir des Noms_Extremement_Long pour aider à la résolution du premier problème, et améliorer la lisibilité du code.
 *
 * Note: Les noms d'espaces de noms ne sont pas sensible à la casse.
 * Note: Les espaces de noms PHP (PHP\...) sont réservés pour l'utilisation interne du langage.
 */
class character /*classe parente de pc npc*/
{
    public string  $name = 'character';
    private int    $id;
    private int    $hp;
    private int    $attack;
    private int    $defense;
    private object $weapon; /*pour avoir tous les attributs de l'arme*/
    private object $magic;/* pour avoir les attributs magic*/


    public function dodge() {//esquiver

    }

    public function parry() {// parer

    }

    /**
     * @param string $name
     * @param int $id
     */
    public
    function __construct(string $name, int $id) {
        $this->name = $name;
        $this->id   = $id;

    }

    /**
     * GETTERS
     */

    public
    function getWeapon(): object {
        return $this->weapon;/*$this représente (l'objet présent) de l'objet de la classe charactere*/
    }

    public
    function getMagic(): object {
        return $this->magic;
    }

    /**
     * @return string
     */
    public
    function getName(): string {
        return $this->name;
    }

    /**
     * @return int
     */
    public
    function getId(): int {
        return $this->id;
    }

    public
    function getHp(): int {
        return $this->hp;
    }

    public
    function getAttack(): int {
        return $this->attack;
    }

    public
    function getDefense(): int {
        return $this->defense;
    }


    /**
     * SETTERS Renvoie toujours void
     */

    public
    function setWeapon(object $weapon): void {
        $this->weapon = $weapon;/*$this représente (l'objet présent) de l'objet de la classe character*/
    }

    /**
     * @param array $weapons
     * @return void
     */
    /*$randomweapons=$weapons[rand(0,count($weapons)-1)]; permet d'afficher le tableau -1 à mettre dans character*/
    public
    function giveWeapon(array $weapons): void {
        $this->weapon = $weapons[rand(0, count($weapons) - 1)];/*N'existe que dans la fonction*/
    }

    /**
     * @param array $magics
     * @return void
     */
    public
    function giveMagic(array $magics): void {
        $this->magic = $magics[rand(0, count($magics) - 1)];
    }

    /**
     * @param int $id
     * @return void
     */
    public
    function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @param string $name
     * @return void
     */
    public
    function setName(string $name): void {
        $this->name = $name;
    }

    public
    function setHp(int $hp): void {
        $this->hp = $hp;
    }

    public
    function setAttack(int $attack): void {
        $this->attack = $attack;

    }

    public
    function setDefense(int $defense): void {
        $this->defense = $defense;
    }

    /**
     * @param object $target
     * @return string
     */
    /*npc et pc seront tous deux utilisés, car il y a héritage*/
    public
    function Attack(object $target): string {

        //to do 5
//todo 2 : modifiez l'attaque (method) afin de prendre en compte l'utilisation de l'arme. QUI SE TROUVE DANS CHARACTER
// les dégâts doivent être égal à une valeur aléatoire comprise entre les dégâts minimum et maximum de l'arme.
// lorsqu'un pc ou un npc est généré, il doit recevoir une capacité magique aléatoire.
        $parry  = false;
        $dodge  = false;
        $damage = 0;
        $def    = 0;

        $result = rand(1, $this->attack) - rand(1, $def);
        $shot   = rand($this->getWeapon()->min_damage, $this->getWeapon()->max_damage);/*this objet -> Possibilité de mettre dans une method dans weapon.php*/
        $magic  = rand($this->getMagic()->min_damage, $this->getMagic()->max_damage);

        if ($this->getMagic()->category == capacity::CAT_OFFENSIVE) {
            //attaque magique
            $damage = $this->getMagic()->getDamage();
        } elseif ($this->getWeapon()->category == capacity::CAT_OFFENSIVE) {
            //attaque weapon
            $damage = $this->getWeapon()->getDamage();
            $def    = $target->getDefense();
            // paré
            if ($this->getWeapon()->type == $this->getWeapon()::CAT_MELEE && $target->getWeapon()->category == capacity::CAT_DEFENSIVE) {
                $parry = true;
            }
            // esquiver
            if ($target->getWeapon()->type == $target->getWeapon()::CAT_RANGED) {
                $dodge = true;
            }
            // defensive magic bonus
            if ($target->getMagic()->category == capacity::CAT_DEFENSIVE) {
                $def += $target->getMagic()->getDefense();
            }
        } else {
            //annule l'attaque
            return $this->name . ' échoue dans son attaque contre ' . $target->name . ' car il ne possède pas de capacité offensive !<br>';
        }
        // si touché
        $result = rand(1, $this->attack) - rand(1, $def);

        if ($result > 0 && $damage) {
            // ça touche
            // calcul des dégâts
            $live   = $target->getHp() - $damage;
            $string = 'touché! ' . $target->name . ' est ';
            if ($live > 0) {
                $string .= 'blessé par ' . $this->getWeapon()->name . ' qui lui inflige ' . $damage . ' points de dégâts! Il lui reste ' . $live . ' points de vie';
            } else {
                $live   = 0;
                $string .= 'mort!';
            }
            $target->setHp($live);
        } elseif ($result < 0) {
            // si ça ne touche pas alors
            $string = 'raté';
        } else {
            $string = 'paré';

        }

        return $this->name . ' attaque ' . $target->name . ' avec une attaque de ' . $this->attack . 'et une capacité magique de ' . $magic . ' face à une défense de ' . $def . '<br> Résultat : ' . $result . ' : ' . $string . '<br>';
        /*retourne le nom de l'attaquant qui attaque la cible avec une attaque de (attribut de l'attaque) face à une défense de ...*/


    }

    /**
     * @param int $parry
     * @return bool
     */
    public function parries(int $parry): bool {
        $oddsParry   = ceil($parry / 10); // ceil arrondi au sup, calcul par 10
        $parryChance = ($oddsParry + 1) * 10; // parer par rapport à la tranche
        $randNumber  = rand(1, 100); // Entre 1 et 100

        // Si nbre inférieur ou égale à la chance alors parade ok
        if ($randNumber <= $parryChance) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param int $dodge
     * @return bool
     */
    public function dodges(int $dodge): bool {
        // Si esquive inférieure à zéro retourne faux
        if ($dodge < 0) {
            return false;
        }
        $oddsDodge   = ceil($dodge / 10);
        $dodgeChance = ($oddsDodge + 1) * 10;
        $numberRand= rand(1,100);

        if($numberRand <= $dodgeChance) {
            return true;
        }else{
            return false;
        }
    }
}