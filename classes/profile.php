<?php

namespace classes;
//Interface toujours abstract, pas de body, only declaration(nom+param)
interface profile
{
    public function getFrontName(): string;
    public function getProfile(array $excluded = []): string;
}