<?php
/**
 * Le trait est une classe abstraite permettant à d'autres classes d'implémenter des méthodes au comportement identique.
 * L'héritage n'est pas nécessaire, l'utilisation du mot "use" associé au nom du trait suffit à l'implémenter dans une classe.
 */
trait profiler {
    public function getProfile(array $excluded = []):string
    {
        $out='';
        foreach ($this as $key => $value) {
            if (!in_array($key, $excluded)){
                $out .="$key => $value\n";
            }
        }
        return $out;
    }
}